# OMniLeads PSTN Gateway

This component implements all the logic involved in the processing of the voice channel.

## Build

To build an image:

```
docker buildx build --file=Dockerfile --tag=$TAG --target=run .
```

Where $TAG is the docker tag you want for image. You can check the version.txt file for the tag.

## Deploy

```
apt update && apt install -y git
curl -fsSL https://get.docker.com -o ~/get-docker.sh
bash ~/get-docker.sh
ln -s /usr/libexec/docker/cli-plugins/docker-compose /usr/bin/
git clone https://gitlab.com/omnileads/omlpstngw.git
cd  omlpstngw/docker-compose/
cp env .env
```

Ahora se debe editar el .env puntualmente los parametros:

```
ASTERISK_HOSTNAME=
SQLHOST=
REDIS_HOSTNAME=
```

Se debe introducir la IP de LAN de la instancia y luego levantar el stack:

```
docker-compose up -d
```

Ahora debemos generar la estructura de MySQL para que Asterisk trabaje en modo ARA

```
docker exec -it pstngw-asterisk bash
pip uninstall alembic
pip install alembic
cd /opt/pstngw/ast-db-manage/
alembic -c ./config.ini upgrade head
```

Finalmente debemos comprobar que Asterisk conecte a MySQL:

```
asterisk -rx 'odbc show'

ODBC DSN Settings
-----------------

  Name:   asterisk
  DSN:    asterisk
    Number of active connections: 1 (out of 1)
    Logging: Enabled
    Number of prepares executed: 17
    Number of queries executed: 17
    Longest running SQL query: SELECT * FROM ps_endpoints WHERE id LIKE ? ORDER BY id (2 milliseconds)
```

Deploy OML Tenant:

```
docker exec  -it pstngw-asterisk  mysql -h 10.124.64.2 -u omnileads -padmin123
use asterisk;
INSERT INTO asterisk.ps_aors (id, max_contacts) VALUES ('fidelio', 1); 
INSERT INTO asterisk.ps_auths (id, auth_type, password, username) VALUES ('fidelio', 'userpass', 'omnileads', 'fidelio');
INSERT INTO asterisk.ps_endpoints (id, transport, aors, auth, context, disallow, allow, direct_media) VALUES ('fidelio', 'oml-transport', 'fidelio', 'fidelio', 'from-oml', 'all', 'alaw', 'no');
```

Deploy PSTN SIP trunk for Tenant:

```
INSERT INTO asterisk.ps_aors (id, contact) VALUES ('fidelio_gw1', 'sip:$SIP_TRUNK_IP:5060'); 
INSERT INTO asterisk.ps_auths (id, auth_type, password, username) VALUES ('fidelio_gw1', 'userpass', 'omnileads', '50505050');
INSERT INTO asterisk.ps_contacts (id, uri, endpoint) VALUES ('fidelio_gw1', 'sip:50505050@$SIP_TRUNK_IP:5060', 'fidelio_gw1');
INSERT INTO asterisk.ps_endpoints (id, transport, aors, auth, context, disallow, allow, direct_media, dtmf_mode, force_rport, outbound_auth, callerid, from_user) VALUES ('fidelio_gw1', 'external-transport', 'fidelio_gw1', 'fidelio_gw1', 'from-pstn', 'all', 'alaw', 'no', 'rfc4733', 'yes', 'fidelio_gw1', '50505050', '50505050');
INSERT INTO asterisk.ps_registrations (id, client_uri, contact_user, endpoint, server_uri, transport, outbound_auth, expiration, max_retries, line) VALUES ('fidelio_gw1', 'sip:50505050@$SIP_TRUNK_IP:5060', '50505050', 'fidelio_gw1', 'sip:50505050@$SIP_TRUNK_IP:5060', 'external-transport', 'fidelio_gw1', 3600, 100, 'yes');
```

Comprobar en CLI de Asterisk que los endpoints y demás configuraciones PJSIP han sido creadas.

```
docker exec  -it pstngw-asterisk asterisk -rvvvvv
pjsip show endpoints
pjsip show aors
pjsip show auths
pjsip show contacts
```
## License

GPLV3