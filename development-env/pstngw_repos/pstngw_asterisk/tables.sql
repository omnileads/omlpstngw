MariaDB [asterisk]> describe extensions;
+----------+--------------+------+-----+---------+----------------+
| Field    | Type         | Null | Key | Default | Extra          |
+----------+--------------+------+-----+---------+----------------+
| id       | bigint(20)   | NO   | PRI | NULL    | auto_increment |
| context  | varchar(40)  | NO   | MUL | NULL    |                |
| exten    | varchar(40)  | NO   |     | NULL    |                |
| priority | int(11)      | NO   |     | NULL    |                |
| app      | varchar(40)  | NO   |     | NULL    |                |
| appdata  | varchar(256) | NO   |     | NULL    |                |
+----------+--------------+------+-----+---------+----------------+



MariaDB [asterisk]> describe iaxfriends;
+-------------------+------------------------------+------+-----+---------+----------------+
| Field             | Type                         | Null | Key | Default | Extra          |
+-------------------+------------------------------+------+-----+---------+----------------+
| id                | int(11)                      | NO   | PRI | NULL    | auto_increment |
| name              | varchar(40)                  | NO   | UNI | NULL    |                |
| type              | enum('friend','user','peer') | YES  |     | NULL    |                |
| username          | varchar(40)                  | YES  |     | NULL    |                |
| mailbox           | varchar(40)                  | YES  |     | NULL    |                |
| secret            | varchar(40)                  | YES  |     | NULL    |                |
| dbsecret          | varchar(40)                  | YES  |     | NULL    |                |
| context           | varchar(40)                  | YES  |     | NULL    |                |
| regcontext        | varchar(40)                  | YES  |     | NULL    |                |
| host              | varchar(40)                  | YES  | MUL | NULL    |                |
| ipaddr            | varchar(40)                  | YES  | MUL | NULL    |                |
| port              | int(11)                      | YES  |     | NULL    |                |
| defaultip         | varchar(20)                  | YES  |     | NULL    |                |
| sourceaddress     | varchar(20)                  | YES  |     | NULL    |                |
| mask              | varchar(20)                  | YES  |     | NULL    |                |
| regexten          | varchar(40)                  | YES  |     | NULL    |                |
| regseconds        | int(11)                      | YES  |     | NULL    |                |
| accountcode       | varchar(80)                  | YES  |     | NULL    |                |
| mohinterpret      | varchar(20)                  | YES  |     | NULL    |                |
| mohsuggest        | varchar(20)                  | YES  |     | NULL    |                |
| inkeys            | varchar(40)                  | YES  |     | NULL    |                |
| outkeys           | varchar(40)                  | YES  |     | NULL    |                |
| language          | varchar(10)                  | YES  |     | NULL    |                |
| callerid          | varchar(100)                 | YES  |     | NULL    |                |
| cid_number        | varchar(40)                  | YES  |     | NULL    |                |
| sendani           | enum('yes','no')             | YES  |     | NULL    |                |
| fullname          | varchar(40)                  | YES  |     | NULL    |                |
| trunk             | enum('yes','no')             | YES  |     | NULL    |                |
| auth              | varchar(20)                  | YES  |     | NULL    |                |
| maxauthreq        | int(11)                      | YES  |     | NULL    |                |
| requirecalltoken  | enum('yes','no','auto')      | YES  |     | NULL    |                |
| encryption        | enum('yes','no','aes128')    | YES  |     | NULL    |                |
| transfer          | enum('yes','no','mediaonly') | YES  |     | NULL    |                |
| jitterbuffer      | enum('yes','no')             | YES  |     | NULL    |                |
| forcejitterbuffer | enum('yes','no')             | YES  |     | NULL    |                |
| disallow          | varchar(200)                 | YES  |     | NULL    |                |
| allow             | varchar(200)                 | YES  |     | NULL    |                |
| codecpriority     | varchar(40)                  | YES  |     | NULL    |                |
| qualify           | varchar(10)                  | YES  |     | NULL    |                |
| qualifysmoothing  | enum('yes','no')             | YES  |     | NULL    |                |
| qualifyfreqok     | varchar(10)                  | YES  |     | NULL    |                |
| qualifyfreqnotok  | varchar(10)                  | YES  |     | NULL    |                |
| timezone          | varchar(20)                  | YES  |     | NULL    |                |
| adsi              | enum('yes','no')             | YES  |     | NULL    |                |
| amaflags          | varchar(20)                  | YES  |     | NULL    |                |
| setvar            | varchar(200)                 | YES  |     | NULL    |                |
+-------------------+------------------------------+------+-----+---------+----------------+


MariaDB [asterisk]> describe musiconhold;
+-------------+-------------------------------------------------------------------+------+-----+---------+-------+
| Field       | Type                                                              | Null | Key | Default | Extra |
+-------------+-------------------------------------------------------------------+------+-----+---------+-------+
| name        | varchar(80)                                                       | NO   | PRI | NULL    |       |
| mode        | enum('custom','files','mp3nb','quietmp3nb','quietmp3','playlist') | YES  |     | NULL    |       |
| directory   | varchar(255)                                                      | YES  |     | NULL    |       |
| application | varchar(255)                                                      | YES  |     | NULL    |       |
| digit       | varchar(1)                                                        | YES  |     | NULL    |       |
| sort        | varchar(10)                                                       | YES  |     | NULL    |       |
| format      | varchar(10)                                                       | YES  |     | NULL    |       |
| stamp       | datetime                                                          | YES  |     | NULL    |       |
| loop_last   | enum('yes','no')                                                  | YES  |     | NULL    |       |
+-------------+-------------------------------------------------------------------+------+-----+---------+-------+

MariaDB [asterisk]> describe musiconhold_entry;
+----------+---------------+------+-----+---------+-------+
| Field    | Type          | Null | Key | Default | Extra |
+----------+---------------+------+-----+---------+-------+
| name     | varchar(80)   | NO   | PRI | NULL    |       |
| position | int(11)       | NO   | PRI | NULL    |       |
| entry    | varchar(1024) | NO   |     | NULL    |       |
+----------+---------------+------+-----+---------+-------+


MariaDB [asterisk]> describe ps_aors;
+----------------------+----------------------------------------------------+------+-----+---------+-------+
| Field                | Type                                               | Null | Key | Default | Extra |
+----------------------+----------------------------------------------------+------+-----+---------+-------+
| id                   | varchar(40)                                        | NO   | PRI | NULL    |       |
| contact              | varchar(255)                                       | YES  |     | NULL    |       |
| default_expiration   | int(11)                                            | YES  |     | NULL    |       |
| mailboxes            | varchar(80)                                        | YES  |     | NULL    |       |
| max_contacts         | int(11)                                            | YES  |     | NULL    |       |
| minimum_expiration   | int(11)                                            | YES  |     | NULL    |       |
| remove_existing      | enum('yes','no')                                   | YES  |     | NULL    |       |
| qualify_frequency    | int(11)                                            | YES  | MUL | NULL    |       |
| authenticate_qualify | enum('yes','no')                                   | YES  |     | NULL    |       |
| maximum_expiration   | int(11)                                            | YES  |     | NULL    |       |
| outbound_proxy       | varchar(40)                                        | YES  |     | NULL    |       |
| support_path         | enum('yes','no')                                   | YES  |     | NULL    |       |
| qualify_timeout      | float                                              | YES  |     | NULL    |       |
| voicemail_extension  | varchar(40)                                        | YES  |     | NULL    |       |
| remove_unavailable   | enum('0','1','off','on','false','true','no','yes') | YES  |     | NULL    |       |
+----------------------+----------------------------------------------------+------+-----+---------+-------+


MariaDB [asterisk]> describe ps_asterisk_publications;
+----------------------+------------------+------+-----+---------+-------+
| Field                | Type             | Null | Key | Default | Extra |
+----------------------+------------------+------+-----+---------+-------+
| id                   | varchar(40)      | NO   | PRI | NULL    |       |
| devicestate_publish  | varchar(40)      | YES  |     | NULL    |       |
| mailboxstate_publish | varchar(40)      | YES  |     | NULL    |       |
| device_state         | enum('yes','no') | YES  |     | NULL    |       |
| device_state_filter  | varchar(256)     | YES  |     | NULL    |       |
| mailbox_state        | enum('yes','no') | YES  |     | NULL    |       |
| mailbox_state_filter | varchar(256)     | YES  |     | NULL    |       |
+----------------------+------------------+------+-----+---------+-------+


MariaDB [asterisk]> describe ps_auths;
+----------------+---------------------------------------+------+-----+---------+-------+
| Field          | Type                                  | Null | Key | Default | Extra |
+----------------+---------------------------------------+------+-----+---------+-------+
| id             | varchar(40)                           | NO   | PRI | NULL    |       |
| auth_type      | enum('md5','userpass','google_oauth') | YES  |     | NULL    |       |
| nonce_lifetime | int(11)                               | YES  |     | NULL    |       |
| md5_cred       | varchar(40)                           | YES  |     | NULL    |       |
| password       | varchar(80)                           | YES  |     | NULL    |       |
| realm          | varchar(40)                           | YES  |     | NULL    |       |
| username       | varchar(40)                           | YES  |     | NULL    |       |
| refresh_token  | varchar(255)                          | YES  |     | NULL    |       |
| oauth_clientid | varchar(255)                          | YES  |     | NULL    |       |
| oauth_secret   | varchar(255)                          | YES  |     | NULL    |       |
+----------------+---------------------------------------+------+-----+---------+-------+

MariaDB [asterisk]> describe ps_contacts;
+----------------------+------------------+------+-----+---------+-------+
| Field                | Type             | Null | Key | Default | Extra |
+----------------------+------------------+------+-----+---------+-------+
| id                   | varchar(255)     | YES  | UNI | NULL    |       |
| uri                  | varchar(511)     | YES  |     | NULL    |       |
| expiration_time      | bigint(20)       | YES  |     | NULL    |       |
| qualify_frequency    | int(11)          | YES  | MUL | NULL    |       |
| outbound_proxy       | varchar(40)      | YES  |     | NULL    |       |
| path                 | text             | YES  |     | NULL    |       |
| user_agent           | varchar(255)     | YES  |     | NULL    |       |
| qualify_timeout      | float            | YES  |     | NULL    |       |
| reg_server           | varchar(255)     | YES  |     | NULL    |       |
| authenticate_qualify | enum('yes','no') | YES  |     | NULL    |       |
| via_addr             | varchar(40)      | YES  |     | NULL    |       |
| via_port             | int(11)          | YES  |     | NULL    |       |
| call_id              | varchar(255)     | YES  |     | NULL    |       |
| endpoint             | varchar(40)      | YES  |     | NULL    |       |
| prune_on_boot        | enum('yes','no') | YES  |     | NULL    |       |
+----------------------+------------------+------+-----+---------+-------+

MariaDB [asterisk]> describe ps_domain_aliases;
+--------+-------------+------+-----+---------+-------+
| Field  | Type        | Null | Key | Default | Extra |
+--------+-------------+------+-----+---------+-------+
| id     | varchar(40) | NO   | PRI | NULL    |       |
| domain | varchar(80) | YES  |     | NULL    |       |
+--------+-------------+------+-----+---------+-------+

MariaDB [asterisk]> describe ps_endpoint_id_ips;
+--------------+------------------+------+-----+---------+-------+
| Field        | Type             | Null | Key | Default | Extra |
+--------------+------------------+------+-----+---------+-------+
| id           | varchar(40)      | NO   | PRI | NULL    |       |
| endpoint     | varchar(40)      | YES  |     | NULL    |       |
| match        | varchar(80)      | YES  |     | NULL    |       |
| srv_lookups  | enum('yes','no') | YES  |     | NULL    |       |
| match_header | varchar(255)     | YES  |     | NULL    |       |
+--------------+------------------+------+-----+---------+-------+

MariaDB [asterisk]> describe ps_endpoints;
+------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+------+-----+---------+-------+
| Field                              | Type                                                                                                                                                                                      | Null | Key | Default | Extra |
+------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+------+-----+---------+-------+
| id                                 | varchar(40)                                                                                                                                                                               | NO   | PRI | NULL    |       |
| transport                          | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| aors                               | varchar(200)                                                                                                                                                                              | YES  |     | NULL    |       |
| auth                               | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| context                            | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| disallow                           | varchar(200)                                                                                                                                                                              | YES  |     | NULL    |       |
| allow                              | varchar(200)                                                                                                                                                                              | YES  |     | NULL    |       |
| direct_media                       | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| connected_line_method              | enum('invite','reinvite','update')                                                                                                                                                        | YES  |     | NULL    |       |
| direct_media_method                | enum('invite','reinvite','update')                                                                                                                                                        | YES  |     | NULL    |       |
| direct_media_glare_mitigation      | enum('none','outgoing','incoming')                                                                                                                                                        | YES  |     | NULL    |       |
| disable_direct_media_on_nat        | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| dtmf_mode                          | enum('rfc4733','inband','info','auto','auto_info')                                                                                                                                        | YES  |     | NULL    |       |
| external_media_address             | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| force_rport                        | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| ice_support                        | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| identify_by                        | varchar(80)                                                                                                                                                                               | YES  |     | NULL    |       |
| mailboxes                          | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| moh_suggest                        | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| outbound_auth                      | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| outbound_proxy                     | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| rewrite_contact                    | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| rtp_ipv6                           | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| rtp_symmetric                      | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| send_diversion                     | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| send_pai                           | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| send_rpid                          | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| timers_min_se                      | int(11)                                                                                                                                                                                   | YES  |     | NULL    |       |
| timers                             | enum('forced','no','required','yes')                                                                                                                                                      | YES  |     | NULL    |       |
| timers_sess_expires                | int(11)                                                                                                                                                                                   | YES  |     | NULL    |       |
| callerid                           | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| callerid_privacy                   | enum('allowed_not_screened','allowed_passed_screened','allowed_failed_screened','allowed','prohib_not_screened','prohib_passed_screened','prohib_failed_screened','prohib','unavailable') | YES  |     | NULL    |       |
| callerid_tag                       | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| 100rel                             | enum('no','required','peer_supported','yes')                                                                                                                                              | YES  |     | NULL    |       |
| aggregate_mwi                      | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| trust_id_inbound                   | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| trust_id_outbound                  | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| use_ptime                          | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| use_avpf                           | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| media_encryption                   | enum('no','sdes','dtls')                                                                                                                                                                  | YES  |     | NULL    |       |
| inband_progress                    | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| call_group                         | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| pickup_group                       | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| named_call_group                   | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| named_pickup_group                 | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| device_state_busy_at               | int(11)                                                                                                                                                                                   | YES  |     | NULL    |       |
| fax_detect                         | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| t38_udptl                          | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| t38_udptl_ec                       | enum('none','fec','redundancy')                                                                                                                                                           | YES  |     | NULL    |       |
| t38_udptl_maxdatagram              | int(11)                                                                                                                                                                                   | YES  |     | NULL    |       |
| t38_udptl_nat                      | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| t38_udptl_ipv6                     | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| tone_zone                          | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| language                           | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| one_touch_recording                | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| record_on_feature                  | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| record_off_feature                 | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| rtp_engine                         | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| allow_transfer                     | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| allow_subscribe                    | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| sdp_owner                          | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| sdp_session                        | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| tos_audio                          | varchar(10)                                                                                                                                                                               | YES  |     | NULL    |       |
| tos_video                          | varchar(10)                                                                                                                                                                               | YES  |     | NULL    |       |
| sub_min_expiry                     | int(11)                                                                                                                                                                                   | YES  |     | NULL    |       |
| from_domain                        | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| from_user                          | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| mwi_from_user                      | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| dtls_verify                        | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| dtls_rekey                         | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| dtls_cert_file                     | varchar(200)                                                                                                                                                                              | YES  |     | NULL    |       |
| dtls_private_key                   | varchar(200)                                                                                                                                                                              | YES  |     | NULL    |       |
| dtls_cipher                        | varchar(200)                                                                                                                                                                              | YES  |     | NULL    |       |
| dtls_ca_file                       | varchar(200)                                                                                                                                                                              | YES  |     | NULL    |       |
| dtls_ca_path                       | varchar(200)                                                                                                                                                                              | YES  |     | NULL    |       |
| dtls_setup                         | enum('active','passive','actpass')                                                                                                                                                        | YES  |     | NULL    |       |
| srtp_tag_32                        | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| media_address                      | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| redirect_method                    | enum('user','uri_core','uri_pjsip')                                                                                                                                                       | YES  |     | NULL    |       |
| set_var                            | text                                                                                                                                                                                      | YES  |     | NULL    |       |
| cos_audio                          | int(11)                                                                                                                                                                                   | YES  |     | NULL    |       |
| cos_video                          | int(11)                                                                                                                                                                                   | YES  |     | NULL    |       |
| message_context                    | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| force_avp                          | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| media_use_received_transport       | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| accountcode                        | varchar(80)                                                                                                                                                                               | YES  |     | NULL    |       |
| user_eq_phone                      | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| moh_passthrough                    | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| media_encryption_optimistic        | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| rpid_immediate                     | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| g726_non_standard                  | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| rtp_keepalive                      | int(11)                                                                                                                                                                                   | YES  |     | NULL    |       |
| rtp_timeout                        | int(11)                                                                                                                                                                                   | YES  |     | NULL    |       |
| rtp_timeout_hold                   | int(11)                                                                                                                                                                                   | YES  |     | NULL    |       |
| bind_rtp_to_media_address          | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| voicemail_extension                | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| mwi_subscribe_replaces_unsolicited | enum('0','1','off','on','false','true','no','yes')                                                                                                                                        | YES  |     | NULL    |       |
| deny                               | varchar(95)                                                                                                                                                                               | YES  |     | NULL    |       |
| permit                             | varchar(95)                                                                                                                                                                               | YES  |     | NULL    |       |
| acl                                | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| contact_deny                       | varchar(95)                                                                                                                                                                               | YES  |     | NULL    |       |
| contact_permit                     | varchar(95)                                                                                                                                                                               | YES  |     | NULL    |       |
| contact_acl                        | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| subscribe_context                  | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| fax_detect_timeout                 | int(11)                                                                                                                                                                                   | YES  |     | NULL    |       |
| contact_user                       | varchar(80)                                                                                                                                                                               | YES  |     | NULL    |       |
| preferred_codec_only               | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| asymmetric_rtp_codec               | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| rtcp_mux                           | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| allow_overlap                      | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| refer_blind_progress               | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| notify_early_inuse_ringing         | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| max_audio_streams                  | int(11)                                                                                                                                                                                   | YES  |     | NULL    |       |
| max_video_streams                  | int(11)                                                                                                                                                                                   | YES  |     | NULL    |       |
| webrtc                             | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| dtls_fingerprint                   | enum('SHA-1','SHA-256')                                                                                                                                                                   | YES  |     | NULL    |       |
| incoming_mwi_mailbox               | varchar(40)                                                                                                                                                                               | YES  |     | NULL    |       |
| bundle                             | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| dtls_auto_generate_cert            | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| follow_early_media_fork            | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| accept_multiple_sdp_answers        | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| suppress_q850_reason_headers       | enum('yes','no')                                                                                                                                                                          | YES  |     | NULL    |       |
| trust_connected_line               | enum('0','1','off','on','false','true','no','yes')                                                                                                                                        | YES  |     | NULL    |       |
| send_connected_line                | enum('0','1','off','on','false','true','no','yes')                                                                                                                                        | YES  |     | NULL    |       |
| ignore_183_without_sdp             | enum('0','1','off','on','false','true','no','yes')                                                                                                                                        | YES  |     | NULL    |       |
| codec_prefs_incoming_offer         | varchar(128)                                                                                                                                                                              | YES  |     | NULL    |       |
| codec_prefs_outgoing_offer         | varchar(128)                                                                                                                                                                              | YES  |     | NULL    |       |
| codec_prefs_incoming_answer        | varchar(128)                                                                                                                                                                              | YES  |     | NULL    |       |
| codec_prefs_outgoing_answer        | varchar(128)                                                                                                                                                                              | YES  |     | NULL    |       |
| stir_shaken                        | enum('0','1','off','on','false','true','no','yes')                                                                                                                                        | YES  |     | NULL    |       |
| send_history_info                  | enum('0','1','off','on','false','true','no','yes')                                                                                                                                        | YES  |     | NULL    |       |
| allow_unauthenticated_options      | enum('0','1','off','on','false','true','no','yes')                                                                                                                                        | YES  |     | NULL    |       |
| t38_bind_udptl_to_media_address    | enum('0','1','off','on','false','true','no','yes')                                                                                                                                        | YES  |     | NULL    |       |
| geoloc_incoming_call_profile       | varchar(80)                                                                                                                                                                               | YES  |     | NULL    |       |
| geoloc_outgoing_call_profile       | varchar(80)                                                                                                                                                                               | YES  |     | NULL    |       |
| incoming_call_offer_pref           | enum('local','local_first','remote','remote_first')                                                                                                                                       | YES  |     | NULL    |       |
| outgoing_call_offer_pref           | enum('local','local_merge','local_first','remote','remote_merge','remote_first')                                                                                                          | YES  |     | NULL    |       |
| stir_shaken_profile                | varchar(80)                                                                                                                                                                               | YES  |     | NULL    |       |
| security_negotiation               | enum('no','mediasec')                                                                                                                                                                     | YES  |     | NULL    |       |
| security_mechanisms                | varchar(512)                                                                                                                                                                              | YES  |     | NULL    |       |
| send_aoc                           | enum('0','1','off','on','false','true','no','yes')                                                                                                                                        | YES  |     | NULL    |       |
| overlap_context                    | varchar(80)                                                                                                                                                                               | YES  |     | NULL    |       |
+------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------+------+-----+---------+-------+

MariaDB [asterisk]> describe ps_globals;
+--------------------------------------------+----------------------------------------------------+------+-----+---------+-------+
| Field                                      | Type                                               | Null | Key | Default | Extra |
+--------------------------------------------+----------------------------------------------------+------+-----+---------+-------+
| id                                         | varchar(40)                                        | NO   | PRI | NULL    |       |
| max_forwards                               | int(11)                                            | YES  |     | NULL    |       |
| user_agent                                 | varchar(255)                                       | YES  |     | NULL    |       |
| default_outbound_endpoint                  | varchar(40)                                        | YES  |     | NULL    |       |
| debug                                      | varchar(40)                                        | YES  |     | NULL    |       |
| endpoint_identifier_order                  | varchar(40)                                        | YES  |     | NULL    |       |
| max_initial_qualify_time                   | int(11)                                            | YES  |     | NULL    |       |
| default_from_user                          | varchar(80)                                        | YES  |     | NULL    |       |
| keep_alive_interval                        | int(11)                                            | YES  |     | NULL    |       |
| regcontext                                 | varchar(80)                                        | YES  |     | NULL    |       |
| contact_expiration_check_interval          | int(11)                                            | YES  |     | NULL    |       |
| default_voicemail_extension                | varchar(40)                                        | YES  |     | NULL    |       |
| disable_multi_domain                       | enum('yes','no')                                   | YES  |     | NULL    |       |
| unidentified_request_count                 | int(11)                                            | YES  |     | NULL    |       |
| unidentified_request_period                | int(11)                                            | YES  |     | NULL    |       |
| unidentified_request_prune_interval        | int(11)                                            | YES  |     | NULL    |       |
| default_realm                              | varchar(40)                                        | YES  |     | NULL    |       |
| mwi_tps_queue_high                         | int(11)                                            | YES  |     | NULL    |       |
| mwi_tps_queue_low                          | int(11)                                            | YES  |     | NULL    |       |
| mwi_disable_initial_unsolicited            | enum('yes','no')                                   | YES  |     | NULL    |       |
| ignore_uri_user_options                    | enum('yes','no')                                   | YES  |     | NULL    |       |
| use_callerid_contact                       | enum('0','1','off','on','false','true','no','yes') | YES  |     | NULL    |       |
| send_contact_status_on_update_registration | enum('0','1','off','on','false','true','no','yes') | YES  |     | NULL    |       |
| taskprocessor_overload_trigger             | enum('none','global','pjsip_only')                 | YES  |     | NULL    |       |
| norefersub                                 | enum('0','1','off','on','false','true','no','yes') | YES  |     | NULL    |       |
| allow_sending_180_after_183                | enum('0','1','off','on','false','true','no','yes') | YES  |     | NULL    |       |
| all_codecs_on_empty_reinvite               | enum('0','1','off','on','false','true','no','yes') | YES  |     | NULL    |       |
+--------------------------------------------+----------------------------------------------------+------+-----+---------+-------+

MariaDB [asterisk]> describe ps_inbound_publications;
+----------------------------+-------------+------+-----+---------+-------+
| Field                      | Type        | Null | Key | Default | Extra |
+----------------------------+-------------+------+-----+---------+-------+
| id                         | varchar(40) | NO   | PRI | NULL    |       |
| endpoint                   | varchar(40) | YES  |     | NULL    |       |
| event_asterisk-devicestate | varchar(40) | YES  |     | NULL    |       |
| event_asterisk-mwi         | varchar(40) | YES  |     | NULL    |       |
+----------------------------+-------------+------+-----+---------+-------+

MariaDB [asterisk]> describe ps_outbound_publishes;
+-------------------+------------------+------+-----+---------+-------+
| Field             | Type             | Null | Key | Default | Extra |
+-------------------+------------------+------+-----+---------+-------+
| id                | varchar(40)      | NO   | PRI | NULL    |       |
| expiration        | int(11)          | YES  |     | NULL    |       |
| outbound_auth     | varchar(40)      | YES  |     | NULL    |       |
| outbound_proxy    | varchar(256)     | YES  |     | NULL    |       |
| server_uri        | varchar(256)     | YES  |     | NULL    |       |
| from_uri          | varchar(256)     | YES  |     | NULL    |       |
| to_uri            | varchar(256)     | YES  |     | NULL    |       |
| event             | varchar(40)      | YES  |     | NULL    |       |
| max_auth_attempts | int(11)          | YES  |     | NULL    |       |
| transport         | varchar(40)      | YES  |     | NULL    |       |
| multi_user        | enum('yes','no') | YES  |     | NULL    |       |
| @body             | varchar(40)      | YES  |     | NULL    |       |
| @context          | varchar(256)     | YES  |     | NULL    |       |
| @exten            | varchar(256)     | YES  |     | NULL    |       |
+-------------------+------------------+------+-----+---------+-------+


MariaDB [asterisk]> describe ps_registrations;
+--------------------------+----------------------------------------------------+------+-----+---------+-------+
| Field                    | Type                                               | Null | Key | Default | Extra |
+--------------------------+----------------------------------------------------+------+-----+---------+-------+
| id                       | varchar(40)                                        | NO   | PRI | NULL    |       |
| auth_rejection_permanent | enum('yes','no')                                   | YES  |     | NULL    |       |
| client_uri               | varchar(255)                                       | YES  |     | NULL    |       |
| contact_user             | varchar(40)                                        | YES  |     | NULL    |       |
| expiration               | int(11)                                            | YES  |     | NULL    |       |
| max_retries              | int(11)                                            | YES  |     | NULL    |       |
| outbound_auth            | varchar(40)                                        | YES  |     | NULL    |       |
| outbound_proxy           | varchar(40)                                        | YES  |     | NULL    |       |
| retry_interval           | int(11)                                            | YES  |     | NULL    |       |
| forbidden_retry_interval | int(11)                                            | YES  |     | NULL    |       |
| server_uri               | varchar(255)                                       | YES  |     | NULL    |       |
| transport                | varchar(40)                                        | YES  |     | NULL    |       |
| support_path             | enum('yes','no')                                   | YES  |     | NULL    |       |
| fatal_retry_interval     | int(11)                                            | YES  |     | NULL    |       |
| line                     | enum('yes','no')                                   | YES  |     | NULL    |       |
| endpoint                 | varchar(40)                                        | YES  |     | NULL    |       |
| support_outbound         | enum('0','1','off','on','false','true','no','yes') | YES  |     | NULL    |       |
| contact_header_params    | varchar(255)                                       | YES  |     | NULL    |       |
| max_random_initial_delay | int(11)                                            | YES  |     | NULL    |       |
| security_negotiation     | enum('no','mediasec')                              | YES  |     | NULL    |       |
| security_mechanisms      | varchar(512)                                       | YES  |     | NULL    |       |
+--------------------------+----------------------------------------------------+------+-----+---------+-------+


MariaDB [asterisk]> describe ps_resource_list;
+-----------------------------+----------------------------------------------------+------+-----+---------+-------+
| Field                       | Type                                               | Null | Key | Default | Extra |
+-----------------------------+----------------------------------------------------+------+-----+---------+-------+
| id                          | varchar(40)                                        | NO   | PRI | NULL    |       |
| list_item                   | varchar(2048)                                      | YES  |     | NULL    |       |
| event                       | varchar(40)                                        | YES  |     | NULL    |       |
| full_state                  | enum('yes','no')                                   | YES  |     | NULL    |       |
| notification_batch_interval | int(11)                                            | YES  |     | NULL    |       |
| resource_display_name       | enum('0','1','off','on','false','true','no','yes') | YES  |     | NULL    |       |
+-----------------------------+----------------------------------------------------+------+-----+---------+-------+

MariaDB [asterisk]> describe ps_subscription_persistence;
+---------------+------------------+------+-----+---------+-------+
| Field         | Type             | Null | Key | Default | Extra |
+---------------+------------------+------+-----+---------+-------+
| id            | varchar(40)      | NO   | PRI | NULL    |       |
| packet        | varchar(2048)    | YES  |     | NULL    |       |
| src_name      | varchar(128)     | YES  |     | NULL    |       |
| src_port      | int(11)          | YES  |     | NULL    |       |
| transport_key | varchar(64)      | YES  |     | NULL    |       |
| local_name    | varchar(128)     | YES  |     | NULL    |       |
| local_port    | int(11)          | YES  |     | NULL    |       |
| cseq          | int(11)          | YES  |     | NULL    |       |
| tag           | varchar(128)     | YES  |     | NULL    |       |
| endpoint      | varchar(40)      | YES  |     | NULL    |       |
| expires       | int(11)          | YES  |     | NULL    |       |
| contact_uri   | varchar(256)     | YES  |     | NULL    |       |
| prune_on_boot | enum('yes','no') | YES  |     | NULL    |       |
+---------------+------------------+------+-----+---------+-------+


MariaDB [asterisk]> describe ps_systems;
+-----------------------------+----------------------------------------------------+------+-----+---------+-------+
| Field                       | Type                                               | Null | Key | Default | Extra |
+-----------------------------+----------------------------------------------------+------+-----+---------+-------+
| id                          | varchar(40)                                        | NO   | PRI | NULL    |       |
| timer_t1                    | int(11)                                            | YES  |     | NULL    |       |
| timer_b                     | int(11)                                            | YES  |     | NULL    |       |
| compact_headers             | enum('yes','no')                                   | YES  |     | NULL    |       |
| threadpool_initial_size     | int(11)                                            | YES  |     | NULL    |       |
| threadpool_auto_increment   | int(11)                                            | YES  |     | NULL    |       |
| threadpool_idle_timeout     | int(11)                                            | YES  |     | NULL    |       |
| threadpool_max_size         | int(11)                                            | YES  |     | NULL    |       |
| disable_tcp_switch          | enum('yes','no')                                   | YES  |     | NULL    |       |
| follow_early_media_fork     | enum('yes','no')                                   | YES  |     | NULL    |       |
| accept_multiple_sdp_answers | enum('yes','no')                                   | YES  |     | NULL    |       |
| disable_rport               | enum('0','1','off','on','false','true','no','yes') | YES  |     | NULL    |       |
+-----------------------------+----------------------------------------------------+------+-----+---------+-------+

MariaDB [asterisk]> describe ps_transports;
+----------------------------+----------------------------------------------------------------+------+-----+---------+-------+
| Field                      | Type                                                           | Null | Key | Default | Extra |
+----------------------------+----------------------------------------------------------------+------+-----+---------+-------+
| id                         | varchar(40)                                                    | NO   | PRI | NULL    |       |
| async_operations           | int(11)                                                        | YES  |     | NULL    |       |
| bind                       | varchar(40)                                                    | YES  |     | NULL    |       |
| ca_list_file               | varchar(200)                                                   | YES  |     | NULL    |       |
| cert_file                  | varchar(200)                                                   | YES  |     | NULL    |       |
| cipher                     | varchar(200)                                                   | YES  |     | NULL    |       |
| domain                     | varchar(40)                                                    | YES  |     | NULL    |       |
| external_media_address     | varchar(40)                                                    | YES  |     | NULL    |       |
| external_signaling_address | varchar(40)                                                    | YES  |     | NULL    |       |
| external_signaling_port    | int(11)                                                        | YES  |     | NULL    |       |
| method                     | enum('default','unspecified','tlsv1','sslv2','sslv3','sslv23') | YES  |     | NULL    |       |
| local_net                  | varchar(40)                                                    | YES  |     | NULL    |       |
| password                   | varchar(40)                                                    | YES  |     | NULL    |       |
| priv_key_file              | varchar(200)                                                   | YES  |     | NULL    |       |
| protocol                   | enum('udp','tcp','tls','ws','wss','flow')                      | YES  |     | NULL    |       |
| require_client_cert        | enum('yes','no')                                               | YES  |     | NULL    |       |
| verify_client              | enum('yes','no')                                               | YES  |     | NULL    |       |
| verify_server              | enum('yes','no')                                               | YES  |     | NULL    |       |
| tos                        | varchar(10)                                                    | YES  |     | NULL    |       |
| cos                        | int(11)                                                        | YES  |     | NULL    |       |
| allow_reload               | enum('yes','no')                                               | YES  |     | NULL    |       |
| symmetric_transport        | enum('yes','no')                                               | YES  |     | NULL    |       |
| allow_wildcard_certs       | enum('yes','no')                                               | YES  |     | NULL    |       |
+----------------------------+----------------------------------------------------------------+------+-----+---------+-------+

MariaDB [asterisk]> describe sippeers;
+--------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------+------+-----+---------+----------------+
| Field              | Type                                                                                                                                                                | Null | Key | Default | Extra          |
+--------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------+------+-----+---------+----------------+
| id                 | int(11)                                                                                                                                                             | NO   | PRI | NULL    | auto_increment |
| name               | varchar(40)                                                                                                                                                         | NO   | UNI | NULL    |                |
| ipaddr             | varchar(45)                                                                                                                                                         | YES  | MUL | NULL    |                |
| port               | int(11)                                                                                                                                                             | YES  |     | NULL    |                |
| regseconds         | int(11)                                                                                                                                                             | YES  |     | NULL    |                |
| defaultuser        | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| fullcontact        | varchar(80)                                                                                                                                                         | YES  |     | NULL    |                |
| regserver          | varchar(20)                                                                                                                                                         | YES  |     | NULL    |                |
| useragent          | varchar(255)                                                                                                                                                        | YES  |     | NULL    |                |
| lastms             | int(11)                                                                                                                                                             | YES  |     | NULL    |                |
| host               | varchar(40)                                                                                                                                                         | YES  | MUL | NULL    |                |
| type               | enum('friend','user','peer')                                                                                                                                        | YES  |     | NULL    |                |
| context            | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| permit             | varchar(95)                                                                                                                                                         | YES  |     | NULL    |                |
| deny               | varchar(95)                                                                                                                                                         | YES  |     | NULL    |                |
| secret             | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| md5secret          | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| remotesecret       | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| transport          | enum('udp','tcp','tls','ws','wss','udp,tcp','tcp,udp')                                                                                                              | YES  |     | NULL    |                |
| dtmfmode           | enum('rfc2833','info','shortinfo','inband','auto')                                                                                                                  | YES  |     | NULL    |                |
| directmedia        | enum('yes','no','nonat','update','outgoing')                                                                                                                        | YES  |     | NULL    |                |
| nat                | varchar(29)                                                                                                                                                         | YES  |     | NULL    |                |
| callgroup          | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| pickupgroup        | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| language           | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| disallow           | varchar(200)                                                                                                                                                        | YES  |     | NULL    |                |
| allow              | varchar(200)                                                                                                                                                        | YES  |     | NULL    |                |
| insecure           | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| trustrpid          | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| progressinband     | enum('yes','no','never')                                                                                                                                            | YES  |     | NULL    |                |
| promiscredir       | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| useclientcode      | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| accountcode        | varchar(80)                                                                                                                                                         | YES  |     | NULL    |                |
| setvar             | varchar(200)                                                                                                                                                        | YES  |     | NULL    |                |
| callerid           | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| amaflags           | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| callcounter        | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| busylevel          | int(11)                                                                                                                                                             | YES  |     | NULL    |                |
| allowoverlap       | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| allowsubscribe     | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| videosupport       | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| maxcallbitrate     | int(11)                                                                                                                                                             | YES  |     | NULL    |                |
| rfc2833compensate  | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| mailbox            | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| session-timers     | enum('accept','refuse','originate')                                                                                                                                 | YES  |     | NULL    |                |
| session-expires    | int(11)                                                                                                                                                             | YES  |     | NULL    |                |
| session-minse      | int(11)                                                                                                                                                             | YES  |     | NULL    |                |
| session-refresher  | enum('uac','uas')                                                                                                                                                   | YES  |     | NULL    |                |
| t38pt_usertpsource | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| regexten           | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| fromdomain         | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| fromuser           | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| qualify            | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| defaultip          | varchar(45)                                                                                                                                                         | YES  |     | NULL    |                |
| rtptimeout         | int(11)                                                                                                                                                             | YES  |     | NULL    |                |
| rtpholdtimeout     | int(11)                                                                                                                                                             | YES  |     | NULL    |                |
| sendrpid           | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| outboundproxy      | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| callbackextension  | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| timert1            | int(11)                                                                                                                                                             | YES  |     | NULL    |                |
| timerb             | int(11)                                                                                                                                                             | YES  |     | NULL    |                |
| qualifyfreq        | int(11)                                                                                                                                                             | YES  |     | NULL    |                |
| constantssrc       | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| contactpermit      | varchar(95)                                                                                                                                                         | YES  |     | NULL    |                |
| contactdeny        | varchar(95)                                                                                                                                                         | YES  |     | NULL    |                |
| usereqphone        | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| textsupport        | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| faxdetect          | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| buggymwi           | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| auth               | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| fullname           | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| trunkname          | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| cid_number         | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| callingpres        | enum('allowed_not_screened','allowed_passed_screen','allowed_failed_screen','allowed','prohib_not_screened','prohib_passed_screen','prohib_failed_screen','prohib') | YES  |     | NULL    |                |
| mohinterpret       | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| mohsuggest         | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| parkinglot         | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| hasvoicemail       | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| subscribemwi       | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| vmexten            | varchar(40)                                                                                                                                                         | YES  |     | NULL    |                |
| autoframing        | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| rtpkeepalive       | int(11)                                                                                                                                                             | YES  |     | NULL    |                |
| call-limit         | int(11)                                                                                                                                                             | YES  |     | NULL    |                |
| g726nonstandard    | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| ignoresdpversion   | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| allowtransfer      | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| dynamic            | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
| path               | varchar(256)                                                                                                                                                        | YES  |     | NULL    |                |
| supportpath        | enum('yes','no')                                                                                                                                                    | YES  |     | NULL    |                |
+--------------------+---------------------------------------------------------------------------------------------------------------------------------------------------------------------+------+-----+---------+----------------+