from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://omnileads:admin123@mariadb/asterisk'  
db = SQLAlchemy(app)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    password = db.Column(db.String(120), nullable=False)
    is_admin = db.Column(db.Boolean, default=False)  # Columna para indicar si es administrador

# Actualiza un usuario existente para que sea administrador
with app.app_context():
    user = User.query.filter_by(username='admin').first() 
    if user:
        user.is_admin = True
        db.session.commit()
