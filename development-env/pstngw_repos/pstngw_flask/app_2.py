import pymysql
from flask import Flask
from flask import render_template
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)

# Configuración de la conexión a MySQL
app.config["SQLALCHEMY_DATABASE_URI"] = "mysql+pymysql://omnileads:admin123d@mariadb/asterisk"

# Creación de la instancia de la base de datos
db = SQLAlchemy(app)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/about")
def about():
    return "Esta es la página Acerca de."

if __name__ == "__main__":
    app.run()
