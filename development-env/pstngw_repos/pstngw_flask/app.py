# app.py
from flask import Flask, render_template, request, redirect, url_for, flash
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import InputRequired, Length
from flask_migrate import Migrate
from flask_bcrypt import Bcrypt
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user

app = Flask(__name__)
app.config['SECRET_KEY'] = 'clave_secreta'
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://omnileads:admin123@mariadb/asterisk'
db = SQLAlchemy(app)
migrate = Migrate(app, db)
bcrypt = Bcrypt(app)
login_manager = LoginManager(app)
login_manager.login_view = 'login'


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True)
    password = db.Column(db.String(120))


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class LoginForm(FlaskForm):
    username = StringField('Usuario', validators=[InputRequired(), Length(min=4, max=80)])
    password = PasswordField('Contraseña', validators=[InputRequired(), Length(min=6, max=120)])


class UserForm(FlaskForm):
    username = StringField('Usuario', validators=[InputRequired(), Length(min=4, max=80)])


@app.route('/')
def index():
    return redirect(url_for('login'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()

    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user)
            return redirect(url_for('dashboard'))

        flash('Credenciales incorrectas', 'danger')

    return render_template('login.html', form=form)


@app.route('/dashboard')
@login_required
def dashboard():
    return render_template('dashboard.html')


@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('login'))


@app.route('/usuarios')
@login_required
def usuarios():
    users = User.query.all()
    return render_template('usuarios.html', users=users)


@app.route('/usuario/nuevo', methods=['GET', 'POST'])
@login_required
def nuevo_usuario():
    form = UserForm()

    if form.validate_on_submit():
        username = form.username.data
        existing_user = User.query.filter_by(username=username).first()
        if not existing_user:
            new_user = User(username=username, password=bcrypt.generate_password_hash('password').decode('utf-8'))
            db.session.add(new_user)
            db.session.commit()
            flash('Usuario creado correctamente', 'success')
            return redirect(url_for('usuarios'))

        flash('El usuario ya existe', 'danger')

    return render_template('nuevo_usuario.html', form=form)


@app.route('/usuario/editar/<int:id>', methods=['GET', 'POST'])
@login_required
def editar_usuario(id):
    user = User.query.get(id)
    form = UserForm(obj=user)

    if form.validate_on_submit():
        user.username = form.username.data
        db.session.commit()
        flash('Usuario editado correctamente', 'success')
        return redirect(url_for('usuarios'))

    return render_template('editar_usuario.html', form=form, user=user)


@app.route('/usuario/borrar/<int:id>')
@login_required
def borrar_usuario(id):
    user = User.query.get(id)
    db.session.delete(user)
    db.session.commit()
    flash('Usuario eliminado correctamente', 'success')
    return redirect(url_for('usuarios'))


if __name__ == '__main__':
    app.run(debug=True)
